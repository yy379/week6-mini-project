use aws_config::load_from_env;
use aws_sdk_dynamodb::{model::AttributeValue, Client};
use lambda_runtime::{service_fn, Error, LambdaEvent};
use serde::Deserialize;
use serde_json::{json, Value};
// tracing
use tracing::{info, Level};
use tracing_subscriber::FmtSubscriber;
// logging
use log::{debug, error, warn};

#[derive(Deserialize)]
struct Request {
    user: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    // Set up the logger
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::TRACE)
        .finish();

    tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");

    env_logger::init();

    error!("{}", "And error occured");
    warn!("{:#?}", "This is important");
    info!("{:?}", "Take note");
    debug!("Something weird occured: {}", "Error");

    let func = service_fn(lambda_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn lambda_handler(event: LambdaEvent<Value>) -> Result<Value, Error> {
    info!("Received event: {:?}", event);

    // Extract the event data from the LambdaEvent
    let request_body = event.payload; // This extracts the actual event data
    let request: Request = serde_json::from_value(request_body)?; // Deserialize the event data into your Request struct

    let config = load_from_env().await;
    let client = Client::new(&config);

    let visit_count = increment_visit_count(&client, &request.user).await?;
    Ok(
        json!({ "message": format!("Hello, {}! You have visited this page {} times.", request.user, visit_count) }),
    )
}

async fn increment_visit_count(client: &Client, user: &str) -> Result<i64, Error> {
    let table_name = std::env::var("TABLE_NAME").expect("TABLE_NAME must be set");

    let get_resp = client
        .get_item()
        .table_name(&table_name)
        .key("user", AttributeValue::S(user.to_string()))
        .send()
        .await?;

    let visit_count = if let Some(item) = get_resp.item {
        item.get("visit_count")
            .and_then(|v| v.as_n().ok())
            .and_then(|s| s.parse::<i64>().ok())
            .unwrap_or(0)
    } else {
        0
    } + 1;

    client
        .put_item()
        .table_name(&table_name)
        .item("user", AttributeValue::S(user.to_string()))
        .item("visit_count", AttributeValue::N(visit_count.to_string()))
        .send()
        .await?;

    Ok(visit_count)
}
