# Week6 Cloud Computing Foundations-DevOps

## Requirements
- Create a Rust AWS Lambda function (or app runner)
- Implement a simple service
- Connect to a database

## Steps to run the project
### Use Cargo to init the project
```bash
cargo init
```

### Add components to main.rs, Cargo.toml, and other files
- In this project, I managed to store username in DynamoDB

### Build the project
```bash
cargo build
```
### Deploy the project
```bash
cargo lambda deploy --region 
```
### Enable X-Ray
![X-Ray](./screenshots/enable-x-ray.png)
### Add permission to the lambda function
![Permission](./screenshots/permission.png)
### Check the logs
![Logs](./screenshots/cloud_logs.png)
### Check Trace in X-Ray
![Trace](./screenshots/trace.png)
## Cargo commands
- `cargo build`: Compiles your project
- `cargo run`: Compiles and runs your project
- `cargo test`: Runs the tests
- `cargo doc`: Builds the documentation
- `cargo update`: Updates your dependencies
- `cargo check`: Checks your code to make sure it compiles but doesn't produce an executable
- `cargo clean`: Removes the target directory

